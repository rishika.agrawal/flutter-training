import 'package:flutter/material.dart';

class FullPicturePage extends StatefulWidget {
  const FullPicturePage({Key? key}) : super(key: key);

  @override
  State<FullPicturePage> createState() => _FullPicturePageState();
}

class _FullPicturePageState extends State<FullPicturePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: InkWell(
          onTap: (){
            Navigator.of(context).pop();
          },
          child: Hero(
            tag: 'Loan',
            child: Image.asset('assets/loan.png',
            height: 160,
            width: 160,),
        ),
        ),
      ),
    );
  }
}
