class BankList {
  String? message;
  Data? data;

  BankList({this.message, this.data});

  BankList.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['message'] = message;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    return data;
  }
}

class Data {
  List<SchemesList>? schemesList;

  Data({this.schemesList});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['schemes_list'] != null) {
      schemesList = <SchemesList>[];
      json['schemes_list'].forEach((v) {
        schemesList!.add(SchemesList.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (schemesList != null) {
      data['schemes_list'] = schemesList!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SchemesList {
  String? isin;
  String? schemeName;
  int? allowed;
  String? ltv;
  String? instrumentType;
  String? schemeType;
  double? price;
  String? lenders;
  String? category;
  String? amcCode;
  String? amcImage;

  SchemesList(
      {this.isin,
        this.schemeName,
        this.allowed,
        this.ltv,
        this.instrumentType,
        this.schemeType,
        this.price,
        this.lenders,
        this.category,
        this.amcCode,
        this.amcImage});

  SchemesList.fromJson(Map<String, dynamic> json) {
    isin = json['isin'];
    schemeName = json['scheme_name'];
    allowed = json['allowed'];
    ltv = json['ltv'];
    instrumentType = json['instrument_type'];
    schemeType = json['scheme_type'];
    price = json['price'];
    lenders = json['lenders'];
    category = json['category'];
    amcCode = json['amc_code'];
    amcImage = json['amc_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['isin'] = isin;
    data['scheme_name'] = schemeName;
    data['allowed'] = allowed;
    data['ltv'] = ltv;
    data['instrument_type'] = instrumentType;
    data['scheme_type'] = schemeType;
    data['price'] = price;
    data['lenders'] = lenders;
    data['category'] = category;
    data['amc_code'] = amcCode;
    data['amc_image'] = amcImage;
    return data;
  }
}
