// import 'package:flutter/material.dart';
// import 'package:untitled/eligibility.dart';
//
// class SearchScheme extends SearchDelegate {
//   @override
//
//   List<Widget>? buildActions(BuildContext context) {
//     return [
//       IconButton(
//         icon: const Icon(Icons.close),
//         onPressed: () {
//           query = "";
//         },
//       )
//     ];
//   }
//
//   @override
//   Widget? buildLeading(BuildContext context) {
//     return IconButton(
//         onPressed: () {
//           Navigator.pop(context);
//         },
//         icon: const Icon(Icons.arrow_back_ios));
//   }
//
//   @override
//   Widget buildSuggestions(BuildContext context) {
//     return Expanded(
//       child: ListView.builder(
//           itemCount: schemesList!.length,
//           scrollDirection: Axis.vertical,
//           shrinkWrap: true,
//           itemBuilder: (BuildContext context, int index) {
//             return Card(
//               elevation: 10,
//               child: Padding(
//                 padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
//                 child: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     Text('${schemesList![index].schemeName}',
//                         style: const TextStyle(
//                             color: Colors.indigo,
//                             fontSize: 18,
//                             fontWeight: FontWeight.bold)),
//                     const SizedBox(height: 10),
//                     Row(
//                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                       children: [
//                         Column(
//                           children: <Widget>[
//                             Text(
//                                 '${schemesList![index].price?.toStringAsFixed(2)}',
//                                 style: const TextStyle(fontSize: 18)),
//                             const Text('QTY',
//                                 style: TextStyle(fontSize: 18)),
//                           ],
//                         ),
//                         // IconButton(onPressed: (){}, icon: const Icon(Icons.remove)),
//                         ElevatedButton(
//                           onPressed: () {},
//                           style: ElevatedButton.styleFrom(
//                               shape: const StadiumBorder()),
//                           child: const Text('  Add +  '),
//                         ),
//                         //  Expanded(
//                         //   child: SizedBox(
//                         //     width: 60,
//                         //     child: TextField(
//                         //       textAlign: TextAlign.center,
//                         //       keyboardType: TextInputType.number,
//                         //       inputFormatters: <TextInputFormatter>[
//                         //         LengthLimitingTextInputFormatter(2),
//                         //         FilteringTextInputFormatter.digitsOnly
//                         //       ],
//                         //       style: const TextStyle(
//                         //         fontSize: 20,
//                         //       ),
//                         //     ),
//                         //   ),
//                         // ),
//                         // IconButton(onPressed: (){}, icon: const Icon(Icons.add)),
//                         CircleAvatar(
//                           // child: InkWell(
//                           //   onTap: (){
//                           //     Navigator.of(context).push(
//                           //       MaterialPageRoute(builder: (context) => const FullPicturePage())
//                           //     );
//                           //   },
//                           //   child: Hero(
//                           //     tag: 'Loan',
//                           child: Image(
//                             image: NetworkImage(schemesList![index]
//                                 .amcImage
//                                 .toString()),
//                             width: 50,
//                             height: 50,
//                           ),
//                         ),
//                       ],
//                     )
//                   ],
//                 ),
//               ),
//             );
//           }),
//     );
//   }
//
//   @override
//   Widget buildResults(BuildContext context) {
//     return const Center(
//       child: Text(''),
//     );
//   }
// }
