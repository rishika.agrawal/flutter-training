import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:untitled/modelBankList.dart';
import 'package:untitled/full_picture.dart';

class Eligibility extends StatefulWidget {
  const Eligibility({Key? key}) : super(key: key);

  @override
  State<Eligibility> createState() => _EligibilityState();
}

class _EligibilityState extends State<Eligibility> {
  // String dropDownValue = 'Demat Account';
  // var items = [
  //   'Item 1',
  //   'Item 2',
  //   'Item 3',
  //   'Item 4',
  //   'Item 5',
  // ];
  bool light = true;
  List<int> quantityNumber = [];

  List<bool> isAddButtonVisible = [];


  Icon customIcon = const Icon(Icons.search);
  Widget customSearchBar = const Text('Eligibility');

  List<SchemesList>? schemesList = [];
  List<SchemesList>? originalSchemeList = [];

  double securityValue = 0.0;
  double eligibleLoanAmount = 0.0;

  Future<dynamic> getBankList() async {
    final String response = await rootBundle.loadString('assets/json.json');
    final jsonData = await json.decode(response);
    BankList bankList = BankList.fromJson(jsonData);
    setState(() {
      schemesList!.addAll(bankList.data!.schemesList!);
      schemesList!.forEach((element) {
        isAddButtonVisible.add(true);
        quantityNumber.add(0);
      });
      originalSchemeList!.addAll(schemesList!);
    });

  }

  Future<dynamic> bottomSheet() async {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: 250,
            child: ElevatedButton(
              child: const Text('Close'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          );
        });
  }

  @override
  void initState() {
    getBankList();
    super.initState();
  }

  void _incrementCounter(int index) {
    setState(() {
      quantityNumber[index]++;
      securityValueText();
    });

  }

  void _decrementCounter(int index) {
    if (quantityNumber[index] > 0) {
      setState(() {
        quantityNumber[index]--;
      });
    }
  }

  applySearch(String value){
    List<SchemesList> filteredList = [];
    originalSchemeList!.forEach((element) {
      if(element.schemeName!.toLowerCase().contains(value.toLowerCase())){
        filteredList.add(element);
      }
    });
    setState(() {
      schemesList!.clear();
      schemesList!.addAll(filteredList);
    });
  }

  securityValueText(){
    securityValue = 0;
    for(int i = 0; i<schemesList!.length; i++) {
        if(quantityNumber[i] != 0){
          securityValue += (quantityNumber[i] * schemesList![i].price!);
          eligibleLoanAmount = securityValue/2;
        }
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: customSearchBar,
          centerTitle: true,
          elevation: 0,
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    if (customIcon.icon == Icons.search) {
                      customIcon = const Icon(Icons.close);
                      customIcon = const Icon(Icons.close);
                      customSearchBar = ListTile(
                        leading: const Icon(
                          Icons.search,
                          color: Colors.black,
                          size: 28,
                        ),
                        title: TextField(
                          onChanged: (value) => applySearch(value),
                          decoration: const InputDecoration(
                            hintText: 'Search...',
                            hintStyle: TextStyle(
                                fontStyle: FontStyle.italic,
                                color: Colors.black),
                            border: InputBorder.none,
                          ),
                        ),
                      );
                    } else {
                      customIcon = const Icon(Icons.search);
                      customSearchBar = const Text('Eligibility');
                      setState(() {
                        schemesList!.clear();
                        schemesList!.addAll(originalSchemeList!);
                      });
                    }
                  });
                },
                icon: customIcon)
          ],
        ),
        body: Column(
          children: <Widget>[
            // Card(
            //   child: Row(
            //     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //     children: <Widget>[
            //       DropdownButton(
            //         hint: const Text('Demat Account'),
            //         items: items.map((String items){
            //           return DropdownMenuItem(
            //             value: items,
            //             child: Text(items));
            //         }).toList(), onChanged: (String? newValue) {
            //           setState(() {
            //             dropDownValue = newValue!;
            //           });
            //       }),
            //       const Text('My Holdings'),
            //       Switch(value: light, onChanged: (bool value){
            //         setState(() {
            //           light = value;
            //         });
            //       }),
            //     ],
            //   ),
            // ),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: ListView.builder(
                    itemCount: schemesList!.length,
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                        child: Card(
                          elevation: 2,
                          child: Padding(
                            padding: const EdgeInsets.fromLTRB(12, 10, 12, 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('${schemesList![index].schemeName}',
                                    style: const TextStyle(
                                        color: Colors.indigo,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                                const SizedBox(height: 16),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                            '${schemesList![index].price?.toStringAsFixed(2)}',
                                            style: const TextStyle(fontSize: 18)),
                                        Text('${quantityNumber[index]} QTY',
                                            style: const TextStyle(fontSize: 18)),
                                      ],
                                    ),
                                    isAddButtonVisible[index]
                                        ? ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            shape: const StadiumBorder()),
                                            onPressed: () {
                                              setState(() {
                                                isAddButtonVisible[index] = false;
                                                securityValueText();
                                              });
                                            },
                                            child: const Text(' Add + '))
                                        : Column(
                                            children: <Widget>[
                                              Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  FloatingActionButton(
                                                      onPressed: () {
                                                        setState(() {
                                                          _decrementCounter(index);
                                                          if (quantityNumber[index] == 0) {
                                                            isAddButtonVisible[index] = true;
                                                            quantityNumber[index] = 1;
                                                          }
                                                          securityValueText();
                                                        });
                                                      },
                                                      mini: true,
                                                      backgroundColor:
                                                          Colors.indigo,
                                                      child:
                                                          const Icon(Icons.remove)),
                                                  SizedBox(
                                                    width: 30,
                                                    child: Center(
                                                      child: Text(
                                                        '${quantityNumber[index]}',
                                                      ),
                                                    ),
                                                  ),
                                                  FloatingActionButton(
                                                      onPressed: () =>
                                                          _incrementCounter(index),
                                                      mini: true,
                                                      backgroundColor:
                                                          Colors.indigo,
                                                      child: const Icon(Icons.add)),
                                                ],
                                              ),
                                              Text(
                                                  'Value: ${(quantityNumber[index] * schemesList![index].price!).toStringAsFixed(2)}'),
                                            ],
                                          ),
                                    CircleAvatar(
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.of(context).push(
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      const FullPicturePage()));
                                        },
                                        child: Hero(
                                          tag: 'Loan',
                                          child: Image(
                                            image: NetworkImage(schemesList![index]
                                                .amcImage
                                                .toString()),
                                            width: 50,
                                            height: 50,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ),
            Container(
              margin: const EdgeInsets.all(10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children:  <Widget>[const Text('Security Value'), Text('₹$securityValue')],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      const Text('Eligible Loan Amount'),
                      Text(
                        '₹$eligibleLoanAmount',
                        style: const TextStyle(
                          color: Colors.green,
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: ElevatedButton(
                      onPressed: () => bottomSheet(),
                      style: ElevatedButton.styleFrom(
                          shape: const StadiumBorder()),
                      child: const Text('View Vault'),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}