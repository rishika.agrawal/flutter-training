import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:untitled/eligibility.dart';

void main() {
  runApp(
    DevicePreview(
      enabled: true,
      builder: (context) => const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            foregroundColor: Colors.white, backgroundColor: Colors.indigo
          ),
        ),
      ),
      title: 'Eligibility',
      debugShowCheckedModeBanner: false,
      home: const Button(),
    );
  }
}

class Button extends StatefulWidget {
  const Button({Key? key}) : super(key: key);

  @override
  State<Button> createState() => _ButtonState();
}

class _ButtonState extends State<Button> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: const Text('Eligibility',style: TextStyle(fontSize: 20)),
      ),
      body: Center(
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              )
          ),
          onPressed: (){
            Navigator.push(context,MaterialPageRoute(builder: (context) => const Eligibility()));
          },
          child: const Text('Eligibility Check',style: TextStyle(fontSize: 25)),
        ),
      ),
    );
  }
}
